use Test::More;
use strict;
use IO::String;

BEGIN {
    require 't/test-lib.pm';
}

my $res;

my $client = LLNG::Manager::Test->new( {
        ini => {
            logLevel                  => 'error',
            authentication            => 'Demo',
            userDB                    => 'Same',
            brutForceProtection       => 0,
            portalMainLogo            => 'common/logos/logo_llng_old.png',
            checkUser                 => 0,
            portalDisplayFavApps      => 1,
            portalDisplayOidcConsents => 1,
            portalDisplayLoginHistory => 1,
            portalDisplayAppslist     => 1,
            portalDisplayLogout       => 1,
        }
    }
);

## Try to authenticate
ok(
    $res = $client->_post(
        '/',
        IO::String->new('user=dwho&password=dwho'),
        length => 23,
        accept => 'text/html',
    ),
    'Auth query'
);
count(1);

my $id = expectCookie($res);
expectRedirection( $res, 'http://auth.example.com/' );

ok(
    $res = $client->_get(
        '/',
        cookie => "lemonldap=$id",
        accept => 'text/html'
    ),
    'Get Menu'
);
count(1);

# Parse Menu
my @menu =
  map m%<li class="nav-item"><a class="nav-link" href="#(.+)?"><span>%g,
  $res->[2]->[0];
ok( scalar @menu == 5, 'Menu with five links found' )
  or print STDERR Dumper($res);
ok( $menu[0] eq 'appslist', 'appslist link found' )
  or print STDERR Dumper( \@menu );
ok( $menu[1] eq 'password', 'password link found' )
  or print STDERR Dumper( \@menu );
ok( $menu[2] eq 'loginHistory', 'loginHistory link found' )
  or print STDERR Dumper( \@menu );
ok( $menu[3] eq 'oidcConsents', 'oidcConsents link found' )
  or print STDERR Dumper( \@menu );
ok( $menu[4] eq 'logout', 'logout link found' )
  or print STDERR Dumper( \@menu );
count(6);

my @stars =
  map m%src="/static/common/icons/star0\.png"/>%g,
  $res->[2]->[0];
ok( scalar @stars == 3, 'Three silver stars found' )
  or print STDERR Dumper($res);
count(1);

## Try to register a favorite app
my $query =
'id=0002-app&uri=http%3A%2F%2Ftest1.example.com%2F&name=Application+Test+1&logo=demo.png&desc=A+simple+application+displaying+authenticated+user';
ok(
    $res = $client->_post(
        '/favapps',
        IO::String->new($query),
        cookie => "lemonldap=$id",
        length => length($query),
        accept => 'text/html',
    ),
    'Register Test1 FavApp query'
);
$query =
'id=0006-app&uri=http%3A%2F%2Fmanager.example.com%2Fnotifications.html&name=Notifications+explorer&logo=database.png&desc=Explore+WebSSO+notifications
';
ok(
    $res = $client->_post(
        '/favapps',
        IO::String->new($query),
        cookie => "lemonldap=$id",
        length => length($query),
        accept => 'text/html',
    ),
    'Register Notifications FavApp query'
);
count(2);

# Parse Page
ok(
    $res = $client->_get(
        '/',
        cookie => "lemonldap=$id",
        accept => 'text/html'
    ),
    'Get Menu'
);
ok( $res->[2]->[0] =~ qr%<img src="/static/common/logos/logo_llng_old.png"%,
    'Found custom Main Logo' )
  or print STDERR Dumper( $res->[2]->[0] );
@stars =
  map m%src="/static/common/icons/star1\.png"/>%g,
  $res->[2]->[0];
ok( scalar @stars == 2, 'Two gold stars found' )
  or print STDERR Dumper($res);
@stars =
  map m%src="/static/common/icons/star0\.png"/>%g,
  $res->[2]->[0];
ok( scalar @stars == 1, 'One silver star found' )
  or print STDERR Dumper($res);

ok(
    $res->[2]->[0] =~
      m%<span trspan="resetFavApps">Reset my favorite Apps\.</span>%,
    'Found resetFavApps button'
) or print STDERR Dumper( $res->[2]->[0] );
count(5);

ok(
    $res = $client->_get(
        '/favapps',
        cookie => "lemonldap=$id",
        accept => 'application/json',
    ),
    'Get FavApps'
);
eval { $res = JSON::from_json( $res->[2]->[0] ) };
ok( not($@), 'Content is JSON' )
  or explain( $res->[2]->[0], 'JSON content' );
ok( $res->{result} == 1, 'FavApps received' );
ok(
    $res->{apps}->[0]->{appuri} =~ m%http://test1.example.com/%,
    'Found favorite Apps -> http://test1.example.com'
) or explain( $res->{apps}, "test1 FavApps NOT registered" );
ok(
    $res->{apps}->[0]->{appdesc} =~
      m%A simple application displaying authenticated user%,
    'Found favorite Apps description'
) or explain( $res->{apps}, "description NOT registered" );
ok( $res->{apps}->[0]->{applogo} =~ m%demo\.png%, 'Found favorite Apps logo' )
  or explain( $res->{apps}, "logo NOT registered" );
ok( $res->{apps}->[0]->{appname} =~ m%Application Test 1%,
    'Found favorite Apps name' )
  or explain( $res->{apps}, "name NOT registered" );
ok( $res->{apps}->[0]->{appid} =~ m%0002-app%, 'Found favorite Apps id' )
  or explain( $res->{apps}, "id NOT registered" );
ok(
    $res->{apps}->[1]->{appuri} =~
      m%http://manager.example.com/notifications.html%,
    'Found favorite Apps -> http://manager.example.com/notifications.html'
) or explain( $res->{apps}, "notifications FavApps NOT registered" );
count(9);

ok(
    $res = $client->_delete(
        '/favapps',
        cookie => "lemonldap=$id",
        accept => 'application/json',
    ),
    'Delete FavApps'
);
count(1);

ok(
    $res = $client->_get(
        '/favapps',
        cookie => "lemonldap=$id",
        accept => 'application/json',
    ),
    'Get FavApps'
);
eval { $res = JSON::from_json( $res->[2]->[0] ) };
ok( not($@), 'Content is JSON' )
  or explain( $res->[2]->[0], 'JSON content' );
ok( $res->{result} == 1,           'FavApps received' );
ok( scalar @{ $res->{apps} } == 0, 'NO favorite Apps found' )
  or explain( $res->{apps}, "FavApps NOT deleted" );
count(4);

$client->logout($id);
clean_sessions();

done_testing( count() );
