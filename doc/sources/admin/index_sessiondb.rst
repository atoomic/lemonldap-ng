Sessions database
=================

.. toctree::
   :maxdepth: 1

   changesessionbackend
   filesessionbackend
   restsessionbackend
   sqlsessionbackend
   ldapsessionbackend
   nosqlsessionbackend
   mongodbsessionbackend
   browseablesessionbackend
   restsessionbackend
   soapsessionbackend
