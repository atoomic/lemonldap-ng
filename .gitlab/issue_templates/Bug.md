### Environment

LemonLDAP::NG version: (version number)

Operating system: (distribution and version)

Web server: (Nginx/Apache/Node.js/...)

### Summary

Summarize the bug encountered concisely

### Logs

```
Include the logs using logLevel = debug if possible. Attach it as file if it's too big
```

### Backends used

For any bug on configuration/sessions storage, give us details on backends

### Possible fixes

