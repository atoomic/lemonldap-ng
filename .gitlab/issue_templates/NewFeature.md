### Summary

Summarize the proposed new feature concisely

### Design proposition

Detail your proposed implementation (interface design, architecture, impact on
current behavior,…)
